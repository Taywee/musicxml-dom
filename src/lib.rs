pub struct LinkAttributes {
    href: String,
    role: Option<String>,
    title: Option<String>,
    show: String,
    actuate: String,
}

pub struct Work {
    work_number: Option<String>,
    work_title: Option<String>,
    opus: Option<LinkAttributes>,
}

pub struct TypedText {
    content: String,
    // keyword
    type_: Option<String>,
}

pub struct Supports {
    type_: bool,
    element: String,
    attribute: Option<String>,
    value: Option<String>,
}

pub enum Encoding {
    EncodingDate(String),
    Encoder(TypedText),
    Software(String),
    EncodingDescription(String),
    Supports(Supports),
}

pub struct Miscellaneous {
    name: String,
    value: String,
}

/**
 * encoding and miscellaneous don't exactly represent the source blocks.  If the encoding or
 * miscellaneous tags exist, but they don't have any contents, they will still be empty vectors.
 * I don't think this is an issue.  I can't think of a reason a parser would want to pay attention
 * to that.
 */
pub struct Identification {
    creator: Vec<TypedText>,
    rights: Vec<TypedText>,
    encoding: Vec<Encoding>,
    source: Option<String>,
    relation: Vec<TypedText>,
    miscellaneous: Vec<Miscellaneous>,
}

pub type Millimeters = i32;
pub type Tenths = i32;

pub struct Scaling {
    millimeters: Millimeters,
    tenths: Tenths,
}

pub struct MarginType {
    Odd,
    Even,
    Both,
}

pub struct PageMargins {
    type_: MarginType,
    left_margin: Tenths,
    right_margin: Tenths,
    top_margin: Tenths,
    bottom_margin: Tenths,
}

pub struct PageLayout {
    // Both must be present if either is present
    page_height: Option<Tenths>,
    page_width: Option<Tenths>,
    page_margins: Vec<PageMargins>,
}

pub struct SystemMargins {
    left_margin: Tenths,
    right_margin: Tenths,
}

pub struct SystemDivider {
    print_object: bool,
}

pub struct SystemDividers {
    left_divider:
    right_divider:
}

pub struct SystemLayout {
    system_margins: Option<SystemMargins>,
    system_distance: Option<Tenths>,
    top_system_distance: Option<Tenths>,
    system_dividers: Option<SystemDividers>,
}

pub struct Defaults {
    scaling: Option<Scaling>,
    page_layout: Option<PageLayout>,
    system_layout: Option<SystemLayout>,
    staff_layout: Option<StaffLayout>,
    appearance: Option<Appearance>,
    music_font: Option<EmptyFont>,
    word_font: Option<EmptyFont>,
    lyric_font: Vec<LyricFont>,
    lyric_language: Vec<LyricLanguage>,
}

pub struct ScoreHeader {
    work: Option<Work>,
    movement_number: Option<String>,
    movement_title: Option<String>,
    identification: Option<Identification>,
    defaults: Option<Defaults>,
}

pub struct ScorePartwise {
    version: String,
    score_header: ScoreHeader,
}

pub enum Score {
    ScorePartwise(ScorePartwise),
    //ScoreTimewise(ScoreTimewise),
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
