# musicxml-dom

Rust musicxml parsing library (WIP).  At the moment this will accept far more
lax than it will output.  It should always output completely compliant MusicXML,
but it will not validate input XML completely pedantically.  Things like order,
for instance, are not strict (though they may be in the future).
